#include <exception>
using namespace std;

#ifndef __Objednavka_h__
#define __Objednavka_h__

// #include "Hrdina.h"
// #include "Mesto.h"

class Hrdina;
class Mesto;
class Objednavka;

class Objednavka
{
	public: Hrdina* m_unnamed_Hrdina_;
	public: Mesto* m_unnamed_Mesto_;

	public: void prepravNaklad(real penize);

	public: void prepravLidi(int pocObyvatel, real penize);

	public: void postavDum(real penize, int pocetDomu);

	public: void postavFirmu(real penize, int pocetFirem);
};

#endif
