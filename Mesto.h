#include <exception>
#include <string>
using namespace std;

#ifndef __Mesto_h__
#define __Mesto_h__

// #include "Objednavka.h"

class Objednavka;
class Mesto;

class Mesto
{
	private: int m_pocObyvatel;
	private: int m_pocDomu;
	private: int m_pocFirem;
	private: string m_jmeno;
	private: int m_lvlMesta;
	public: Objednavka* m_unnamed_Objednavka_;

	public: int getpocetDomu(int pocDomu);

	public: int getPocetFirem(int pocetFirem);

	public: void vypisMesta();

	public: void printInfo();

	public: int lvlUp(int lvlMesta);
};

#endif
